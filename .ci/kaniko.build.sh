#! /bin/sh

destination=docker.io/lumy/bashug:ci

# shellcheck disable=SC2086
/kaniko/executor \
  --cache \
  --context "${KANIKO_CTX}" \
  --dockerfile "${KANIKO_DOCKERFILE}" \
  --destination "${destination}"
