#! /bin/sh

echo "Hello, $GITLAB_USER_LOGIN. Auto building docker image with kaniko"

mkdir -p /kaniko/.docker

cat <<EOF >/kaniko/.docker/config.json
{
  "auths": {
    "$CI_REGISTRY": {
      "username": "$CI_REGISTRY_USER",
      "password": "$CI_REGISTRY_PASSWORD"
    }
  }
}
EOF
