from .src import func_tab
import argparse


def parse_args():
    """TODO a better argparse where all commands would be separate for better help ? using namespace ?
    """
    parser = argparse.ArgumentParser(description='Play the bash game.')
    parser.add_argument('command',
                        type=str,
                        choices=func_tab.keys(),
                        help='command to be runned')
    parser.add_argument('-d', '--difficulty',
                        type=str,
                        action='store',
                        default="easy",
                        choices=["easy", "medium", "hard"],
                        help='level of difficulty when using init command')

    return parser.parse_args()


def main(args):
    # We could try to load config or None
    func_tab[args.command](args.difficulty)


if __name__ == '__main__':
    args = parse_args()
    main(args)
