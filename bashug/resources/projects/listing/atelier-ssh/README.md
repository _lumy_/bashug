
# Atelier SSH


J'attends de vous un message chiffré avec ma clef publique.
Message auquel je répondrais et vous aussi.

Donc je m'attends a 3 échanges entre nous:
 
  - vous -> lumy
  - lumy -> vous
  - vous -> lumy

Derniers Délai de rendu. vendredi 12h (midi)

## Explication ssh

[Parcequ'une video vaut parfois mieux que mes explications (ceci est un lien)](https://www.youtube.com/watch?v=QGixbJ9prEc)

## Creation clef ssh

Si vous n'avez pas de clef ssh, c'est le moment d'en créer une (c'est une bonne idée de mettre une
passphrase au cas où vous perdriez votre clef ou si elle se retrouvait sur internet). Par défaut une clef est générée dans le dossier
`.ssh` de votre home
Dans votre wsl2:

``` bash
$ ssh-keygen -b 2048
[...]
$ ls ~/.ssh/
id_rsa id_rsa.pub
```


## Echange clef publique

Postez votre clef publique sur discord (`id_rsa.pub`) channel #rendu avec un ping sur lumy (@lumy).
Je ferais de meme. Prenez donc le temps de télécharger la mienne.

## Chiffrer avec clef publique

Malheureusement un fois la clef publique en main elle n'est pas tout à fait utilisable par
`openssl`. Nous allons donc devoir appliquer une petite opération.
``` bash
$ ssh-keygen -f ~/.ssh/id_rsa.pub -e -m pkcs8 > ~/.ssh/id_rsa.pub.pkcs8
# Voila. Maintenant on peut chiffrer des messages.
$ cat message.txt | openssl rsautl -encrypt -pubin -inkey ~/.ssh/id_rsa_pub.pkcs8 > message.enc
```

## Déchiffrer avec clef privee

Pour déchiffrer avec la clef privée:

``` bash
cat message.enc | openssl rsautl -decrypt -inkey ~/.ssh/id_rsa
```
