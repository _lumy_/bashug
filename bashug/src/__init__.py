from .entrypoints import init, reset_game, start_game, check, hint

# TODO add the command reset, that would be diff from init
# init start the game if it has never been start (and would be used to change from easy to medium and hardcore)
# while reset would reset the current directory for the question
func_tab = {"init": lambda x: init(x), "reset": lambda _: reset_game(), "start": lambda _: start_game(), "check": lambda _: check(), "hint": lambda _: hint()}
