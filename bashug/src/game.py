import os
from .helper import log
from .helper.path import _rmtree, write_conf, create_folder, \
    load_config, open_from_pex
from .helper.zip import extract_from_pex, open_from_pex

def clean_exec_env(env):
    """Remove everything env"""
    log.debug(f"Removing {env}")
    _rmtree(env)

def create_default_config_file(difficulty):
    path = f"{os.environ['BASHUG_HOME']}/.simplon"
    workdir = f"{os.environ['BASHUG_HOME']}/{os.environ['BASHUG_WORKDIR']}"
    create_folder(path)
    default = {
        "difficulty": difficulty,
        "conf_path": f'{path}/bashug.config',
        'workdir': workdir,
        difficulty: {
            "lvl": 0,
        }
    }
    default.update(load_config())
    default['difficulty'] = difficulty
    if difficulty not in default:
        default[difficulty] = {"lvl": 0}
    bashg_path = os.environ.get("BASHUG_BIN", "plz-out/bin/bashug/bashug.pex")
    default[difficulty].update(open_from_pex(bashg_path, f"bashug/resources/{difficulty}.yaml"))
    write_conf(default)
    return default

def create_env_work(conf, path):
    create_folder(path, clean=True)
    HEADER = """All answer are to be provided in a file named answer.sh\n\n"""
    lvl = conf.question
    with open(f"{path}/README", "w") as f:
        f.write(HEADER)
        f.write(lvl.question)
        f.write("\n")
    log.debug(f"Question {lvl}")
    #if "start" in lvl:
        # start process in background and replace the pid
        # The need for a models/view here is strong
        # where view would wrote the models or print it.
    if hasattr(lvl, "folders"):
        for folder in lvl['folders']:
            bashg_path = os.environ.get("BASHUG_BIN",
                                        "plz-out/bin/bashug/bashug.pex")
            log.debug(f"Extract {folder} to {path}")
            extract_from_pex(bashg_path,
                             f"bashug/resources/{folder}",
                             f"{path}/",
                             remove_path="bashug/resources/projects")
