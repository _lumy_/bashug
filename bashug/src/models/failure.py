
class Failure(Exception):
    def __init__(self, string, stop=False):
        super(Failure, self).__init__()
        self.msg = "{red}Failed to answer. Please read the README or use the hint.{reset}\n"
        self.msg += "{red}%s{reset}" % string
        self.stop = stop

    def format(self, **kwargs):
        return self.msg.format(**kwargs)
