from bashug.src.helper.path import open_from_pex


class QuestionsList():
    def __init__(self, bin_path, difficulties):
        self._questions = {}
        self.lvl_introduction = ""
        for diff in difficulties:
            quest_list = open_from_pex(bin_path,
                                       f"bashug/resources/{diff}.yaml")
            self.lvl_introduction = quest_list.pop("level_introduction")
            self._questions[diff] = [
                Question(c) for c in quest_list['exercices']
            ]

    def __getitem__(self, index):
        return self._questions[index]


class Question():
    def __init__(self, question_json):
        for k, v in question_json.items():
            setattr(self, k, v)

    def __getitem__(self, key):
        return getattr(self, key)

    def format(self, attr, config, builtins):
        pass
