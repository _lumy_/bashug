
class Cmd():
    def __init__(self, entries):
        self._bin = None
        self._soptions = []
        self._coptions = []
        self._args = []
        if len(entries) == 0:
            raise Failure("no command provided. File is emtpy\n", True)
        self._parse(entry)

    def _parse(self, entry):
        self._bin = entry.pop(0)
        for arg in entry:
            if arg.startswith("-"):
                self.add_option(arg)
            else:
                self.add_arg(arg)



    def __str__(self):
        return f"""{self._bin}
{self._soptions}
{self._coptions}
{self._args}
"""
    def add_option(self, option):
        if option.startswith("--"):
            self._coptions.append(option)
            return
        option = option[1:] # remove -
        for a in option:
          self._soptions.append(f"-{a}")

    def add_arg(self, arg):
        self._args.append(arg)

    def __eq__(self, obj):
        if isinstance(obj, Cmd):
            if self._bin == obj._bin and \
               self._soptions.sort() == obj._soptions.sort() and \
               self._coptions.sort() == obj._coptions.sort() and \
               self._args.sort() == obj._args.sort():
                return True
        return False
