# manage config file
from .config import config
from .helper import log
from .runtime import compare_process
from .helper.pack import load, unload
from .game import create_default_config_file, create_env_work, clean_exec_env
from .helper.path import load_config, open_file, write_conf
from .views import print_default, print_failure, print_level_passed, print_obj
from .models.failure import Failure

def init(difficulty):
    config.difficulty = difficulty
    print_default(config.questions.lvl_introduction)
    start_game()

def reset_game():
    clean_exec_env(config.workdir)
    start_game()

def start_game():
    """Load the level"""
    create_env_work(config, config.workdir)
    print_default("[{red}Q{config.level}{reset}]:{blue}{config.question.question}{reset}\n{underline}Go into {config.workdir} and find this question and more information in the README.{reset}\n", config=config)


def hint(conf=None):
    header =  "Bashug Hint. Don't ask too much"
    print_default("{red}{underline}{header}{reset}\n=> {green}{config.question.hint}{reset}\n", header=header, config=config)
    return 0


def _check():
    """TODO: Add colors to output."""
    answer = open_file(f"{config.workdir}/answer.sh")
    exo = config.question
    if answer is not None:
        compare_process(exo.solutions, answer.strip(), config)

        # except Exception as e:
            # print(e)
            # failure += e.msg
    else:
        raise Failure("{red}{underline}No file answer.sh provided.{reset}", True)

    # if answer and exo['trigger'] in answer:
        # failure += "\n{green}One more HINT: %s{reset}" % exo["subhint"]
    # print_failure(failure, config=config)
    print("Gonna raise Error")
    raise Failure("Unkown for now")

def check():
    try:
      obj = _check()
    except Exception as e:
        obj = e
    print_obj(obj, config)
    if config.register(obj):
        # print_level_passed(config)
        start_game()
