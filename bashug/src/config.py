import configparser
import os
import atexit
from .helper import log
from .helper.path import create_folder, open_from_pex
from .models.questions import QuestionsList
from .models.failure import Failure

class Config(configparser.ConfigParser):
    def __init__(self):
        self._config_path = os.environ.get("BASHUG_CONFIG", "/opt/.bashug/.config")
        self._workdir = f"{os.environ['BASHUG_HOME']}/{os.environ['BASHUG_WORKDIR']}"
        self._bin_path = os.environ.get("BASHUG_BIN", "plz-out/bin/bashug/bashug.pex")
        self._difficulties = ["easy", "medium", "hard"]
        super(Config, self).__init__()
        self.read(self.config_path)
        if len(self.sections()) == 0:
            self._create_config()
        self._questions = QuestionsList(self._bin_path, self._difficulties)

        # when object is destroy actually write content of config in a file
        atexit.register(self._clean_)

    def register(self, obj):
        if isinstance(obj, (Failure, Exception)):
            self[self["GAME"]["difficulty"]]["tried"] = str(self.tried + 1)
            return False
        self.level += 1
        self[self["GAME"]["difficulty"]]["score"] = str(self.score + 1)
        return True
    @property
    def difficulty(self):
        return self["GAME"]["difficulty"]
    @difficulty.setter
    def difficulty(self, val):
        self["GAME"]["difficulty"] = val
    @property
    def workdir(self):
        return self._workdir
    @property
    def config_path(self):
        return self._config_path
    @property
    def level(self):
        """Get Level (question number) value"""
        return int(self[self["GAME"]["difficulty"]]["lvl"])
    @level.setter
    def level(self, val):
        """Set Level (question number) value"""
        self[self["GAME"]["difficulty"]]["lvl"] = str(val)
    @property
    def score(self):
        """Get Score for the level"""
        return int(self[self["GAME"]["difficulty"]]["score"])
    @property
    def tried(self):
        """Get Score for the level"""
        return int(self[self["GAME"]["difficulty"]]["tried"])
    @property
    def question(self):
        """Return the current question for the lvl and difficulty"""
        diff = self["GAME"]["difficulty"]
        lvl = int(self[diff]["lvl"])
        return self._questions[diff][lvl]
    @property
    def questions(self):
        "Return all question from current difficulty"
        return self._questions # [self["GAME"]["difficulty"]]

    def _create_config(self, difficulty="easy"):
        self['GAME'] = {
            "difficulty": difficulty,
            "conf_path": self._config_path,
            'workdir': self._workdir,
        }
        for diff in self._difficulties:
          self[diff] = {
              "lvl": 0,
              "score":0,
              "tried": 0,
              "oneshot": 0,
           }

    def _clean_(self):
        create_folder(os.path.dirname(self.config_path))
        with open(self.config_path, 'w') as configfile:
            self.write(configfile)

config = Config()
