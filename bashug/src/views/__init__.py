from blessings import Terminal
from bashug.src.helper.path import open_file
from bashug.src.models.failure import Failure

term = Terminal()

builtins = {
    "blue": str(term.blue),
    "red": str(term.red),
    "green": str(term.green),
    "underline": str(term.underline),
    "reset": str(term.normal),
    "bold": str(term.bold)
}

def print_obj(obj, config):
    if isinstance(obj, Failure):
        print_failure(obj, config=config)

def print_failure(string, **kwargs):
    with term.location(0, term.height - 1):
        print(string.format(**kwargs, **builtins))

def print_level_passed(config):
    string = "{green}Level Passed ! Good job. Score is {blue}{config.score}{reset}"
    answer, newline, solutions = "", "", ""
    if 'explaination' in config.question:
        answer = open_file(f"{config.workdir}/answer.sh").strip()
        solutions = "\n=> ".join(config.question['solutions'])
        string = """{blue}Your solution worked.{reset} {answer}\n{green}But Other solutions existed{reset}:
=> {solutions}\n{green}Explaination: {blue}{config.question[explaination]}{reset}"""
    print_default(string, config=config, answer=answer, solutions=solutions)

def print_default(string, **kwargs):
    """
    :args string: not formatted string.
    :args kwargs: all argument for format string except colors ones.
    """
    with term.location(0, term.height - 1):
        print(string.format(**kwargs, **builtins))
