import subprocess
import os
import datetime
from .models.cmd import Cmd
from .game import create_env_work, clean_exec_env
from .helper import parse_builtins, log

##
# TODO: To Avoid error we should exec both command in a new clean env/folders.
##

def compare_process(cmdlines_solutions, cmdline_check, config):
    cmd_failed = None
    cmdlines_solutions = parse_builtins(cmdlines_solutions)
    log.debug(f"builtines parsed: {cmdlines_solutions}")
    procs_solutions = [_bash_exec(x, config) for x in cmdlines_solutions]
    proc_check = _bash_exec(cmdline_check, config)
    try:
        _compare_cmd(procs_solutions, proc_check)
    except Exception as e:
        cmd_failed = e
        if cmd_failed.stop == True:
            raise cmd_failed
    cmd_failed = _compare_std(procs_solutions, proc_check, "stdout",
                                cmd_failed)
    cmd_failed = _compare_std(procs_solutions, proc_check, "stderr",
                                cmd_failed)
    if cmd_failed != None:
        cmd_failed.msg += "======================"
        raise cmd_failed


def _bash_exec(cmd, config):
    try:
        create_env_work(config, "/tmp/exec_bashug/procs")
        os.chdir("/tmp/exec_bashug/procs")
        log.debug(f"Executing in {cmd} in /tmp/exec_bashug/procs")
        ret = subprocess.run(cmd,
                             capture_output=True,
                             timeout=10,
                             shell=True,
                             cwd="/tmp/exec_bashug/procs")
        clean_exec_env("/tmp/exec_bashug")
        return ret
    except subprocess.TimeoutExpired as e:
        clean_exec_env("/tmp/exec_bashug")
        e = Exception()
        e.args = cmd
        e.msg = "Your command didn't finish in less than 10sec which raised an ExceptionTimeout\n"
        raise e


def _compare_std(procs_s, proc_c, attr, err):
    if any(
            getattr(proc_s, attr) == getattr(proc_c, attr)
            for proc_s in procs_s):
        return err
    msg = f"""########################################
# Error on standart output: {attr}     #
########################################
# Output excepted:                     #
########################################
"""
    header = """\n########################################
# Output provided by your command:     #
########################################
"""
    msg += f"\n{getattr(procs_s[0], attr).decode('utf-8')}{header}{getattr(proc_c, attr).decode('utf-8')}"
    if err == None:
        err = Exception()
        err.msg = msg
    else:
        err.msg += msg
    return err


## Compare cmdline

def _compare_cmd(procs_sol, proc_c):
    cmd_chk = Cmd(proc_c.args.split())
    log.debug(f""" => {cmd_chk}""")
    for sol in procs_sol:
        cmd_sol = Cmd(sol.args.split())
        log.debug(f""" <= {cmd_sol}""")
        if cmd_sol == cmd_chk:
            return None
    e = Exception()

    e.msg = "Your Command doens't match the excepting command.\n"
    raise e
