from .builtins import _parse_builtin
from .builtins import tomorrow
import logging
import os

log = logging.getLogger(__name__)
if os.environ.get("BASHUG_LOG_LEVEL", "INFO").upper() == "DEBUG":
    logging.basicConfig(level=logging.DEBUG, format='--> %(message)s')
    log.debug("Debug Activated")
else:
    logging.basicConfig(level=logging.WARNING, format='--> %(message)s')

def parse_builtins(cmdlines):
    ret = []
    log.debug(f"Parsing cmdlines: {cmdlines}")
    for cmdline in cmdlines:
      if "{{" in cmdline and "}}" in cmdline:
          ret.append(_parse_builtin(cmdline))
      else:
          ret.append(cmdline)
    return ret
