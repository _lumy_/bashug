import os
import zipfile
from shutil import rmtree
from yaml import load, Loader
from . import log
from .utils import safe_rm

def open_from_pex(zippath, filepath):
    with zipfile.ZipFile(zippath) as zipf:
        with zipf.open(filepath) as f:
            return load(f.read(), Loader=Loader)


def extract_from_pex(zippath, folderpath, outpath, remove_path=None):
    """Exctract from pex file. will `remove_path` from `folder_path` so the output would like to `folderpath`/`outpath - remove_path`"""
    log.debug(f"extracting {folderpath} to {outpath}")
    ret = _extract_from_pex(zippath, folderpath, outpath)
    log.debug(f"<= extracted {folderpath} in {outpath}")
    if remove_path:
        log.debug(f"Removing {remove_path} from {outpath}/{folderpath}")
        _correct_folder(ret, outpath, folderpath.replace(remove_path, ''),
                        remove_path)
        log.debug(f"Removing {outpath}/bashug")
        rmtree(f"{outpath}/bashug")
        for root, dirs, files in os.walk(outpath):
            if ".keep" in files or "__init__.py" in files:
                safe_rm(f"{root}/.keep")
                safe_rm(f"{root}/__init__.py")


def _correct_folder(list_file, outpath, target_folder, remove):
    if target_folder.startswith("/"):
        target_folder = target_folder[1:]
    log.debug(f"{list_file} {outpath} {target_folder} {remove}")
    if os.path.isdir(f"{outpath}/{target_folder.split('/')[0]}"):
        log.debug(f"Removing {outpath}")
        rmtree(f"{outpath}")

    for file in list_file:
        oldpath = file
        file = file.replace(remove, "")
        log.debug(f"{oldpath} to {outpath}/{file}")
        if os.path.isdir(oldpath):
            if not os.path.isdir(f"{outpath}/{file}"):
                os.makedirs(f"{outpath}/{file}")
        else:
            os.rename(oldpath, f"{outpath}/{file}")

def _extract_from_pex(zippath, folderpath, outpath):
    ret = []
    with zipfile.ZipFile(zippath) as zipf:
        os.chdir(outpath)
        if os.path.isdir(f"{outpath}/{os.path.basename(folderpath)}"):
            log.debug(f"Remove tree => {outpath}/{os.path.basename(folderpath)}")
            rmtree(f"{outpath}/{os.path.basename(folderpath)}")
        elif os.path.isfile(f"{outpath}/{os.path.dirname(folderpath)}"):
            log.debug(f"Remove tree file => {outpath}/{os.path.dirname(folderpath)}")
            os.remove(f"{outpath}/{os.path.dirname(folderpath)}")
        l = [x for x in zipf.namelist() if folderpath in x]
        log.debug(f"extracting list {l}")
        for file in l:
            try:
                log.debug(f"extracting {file}")
                a = zipf.extract(file, "")
                ret.append(a)
            except:  # Most probably a folder instead of a file.
                pass
    return ret
