import json
import os


def load():
    if os.path.isfile("{}/.simplon/bash_config".format(os.environ["HOME"])):
        with open("{}/.simplon/bash_config".format(os.environ["HOME"]),
                  "r") as f:
            return json.loads(f.read())
    with open("{}/.simplon/bash_config".format(os.environ["HOME"]), "w") as f:
        f.write(json.dumps({"lvl": 0}))
    return {"lvl": 0}


def unload(content):
    with open("{}/.simplon/bash_config".format(os.environ["HOME"]), "w") as f:
        return f.write(json.dumps(content))
