import os
from . import log
from shutil import rmtree

def safe_rm(path):
    try:
        rmtree(path)
    except:
        pass
    try:
        os.remove(path)
    except:
        pass

def _rmtree(path):
    log.debug(f"Listing {path}")
    for p in os.listdir(path):
        log.debug(f"Removing {p}")
        p = os.path.join(path, p)
        safe_rm(p)
