import os
import copy
import json
from .utils import _rmtree
from .zip import open_from_pex
from . import log

def create_folder(path, clean=False):
    if clean and os.path.isdir(path):
        _rmtree(path)
    if not os.path.isdir(path):
        os.makedirs(path)


def write_conf(conf):
    conf = copy.deepcopy(conf)
    try:
        conf[conf['difficulty']].pop('exercices')
        conf[conf['difficulty']].pop('level_introduction')
    except:
        pass
    with open(conf['conf_path'], "w") as f:
        f.write(json.dumps(conf))


def open_file(filepath):
    try:
        with open(filepath, 'r') as f:
            return f.read()
    except:
        return None

def load_config():
    conf = {}
    path = f"{os.environ['BASHUG_HOME']}/.simplon/bashug.config"
    bashg_path = os.environ.get("BASHUG_BIN", "plz-out/bin/bashug/bashug.pex")
    with open(path, "r") as f:
        conf = json.load(f)
        diff = conf['difficulty']
        conf[diff].update(open_from_pex(bashg_path, f"bashug/resources/{diff}.yaml"))
        return conf
