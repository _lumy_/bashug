import datetime

def tomorrow():
    tom = datetime.date.today() + datetime.timedelta(days=1)
    return f"{tom.strftime('%m%d')}1200"

builtins = {
    "tomorrow": tomorrow,
}

def _parse_builtin(cmdline):
    start, end = cmdline.find("{{"), cmdline.find("}}")
    func = cmdline[start+2:end-1].strip().replace("()", "")
    cmd_ret = builtins[func]()
    cmdline = f"{cmdline[:start]}{cmd_ret}{cmdline[end+2:]}"
    return cmdline
