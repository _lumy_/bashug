# Tutorial Bash

## How to

We use pleaze to build the `bashug` image.

Your can download the image from

``` bash
 $ docker run  -it lumy/bashug
 [...]
 bashug $ bashug -d easy init
 bashug $ bashug --help
 bashug $ alias
```

## Env variables:

### mandatory

BASHUG_BIN
BASHUG_HOME
BASHUG_WORKDIR

#### optionals

BASHUG_CONFIG
BASHUG_LOG_LEVEL

## Binary

## TODO

v0.1.0: easy level

v0.2.0:
  - rewrite parseargs so -d options is only for init.
  - refactor for model/view.
  - Colors output: Finish the failure part, that should store into an object Failure that should be
    printed in the view.
  - print/manage score and offer user to changes level. find a way to mark level as done.
  - write ~40 medium exercices
  - write ~40 hardcore exercices

V1.0.0:
 - detect when all level are finished.
 - write a final level (inspired from githug) that would invite the user to participate to the repo.
